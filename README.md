# Installing OnlyOffice+SSL with docker-compose.

## Quick Installation

**Before starting the instance, direct the domain (subdomain) to the ip of the server where OnlyOffice will be installed!**

## 1. OnlyOffice Server Requirements
From and more
- 2 CPUs
- 2 RAM 
- 10 Gb 

Run for Ubuntu 22.04

``` bash
sudo apt-get purge needrestart
```

Install docker and docker-compose:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/onlyoffice-ssl-docker-compose/-/raw/master/setup.sh | sudo bash -s

```

Download onlyoffice instance:


``` bash
curl -s https://gitlab.com/6ministers/business-apps/onlyoffice-ssl-docker-compose/-/raw/master/download.sh | sudo bash -s onlyoffice
```

If `curl` is not found, install it:

``` bash
$ sudo apt-get install curl
# or
$ sudo yum install curl
```

Go to the catalog

``` bash
cd onlyoffice
```

To change the domain in the `Caddyfile` to your own

``` bash
https://onlyoffice.domain.com:443 {
    reverse_proxy 127.0.0.1:2291
	# tls admin@example.org
	encode zstd gzip
	file_server
...	
}
```

**Run onlyoffice:**

``` bash
docker-compose up -d
```

Then open `https://onlyofice.domain.com:` to access OnlyOffice

On this page, you will need to specify the settings for your installation.

A random secret is generated automatically if a custom secret has not been added during installation. To obtain the default secret, run this command:
``` bash
 sudo docker exec container_name /var/www/onlyoffice/documentserver/npm/json -f /etc/onlyoffice/documentserver/local.json 'services.CoAuthoring.secret.session.string'
```

## OnlyOffice container management

**Run onlyoffice**:

``` bash
docker-compose up -d
```

**Restart**:

``` bash
docker-compose restart
```

**Restart**:

``` bash
sudo docker-compose down && sudo docker-compose up -d
```

**Stop**:

``` bash
docker-compose down
```

## Documentation

