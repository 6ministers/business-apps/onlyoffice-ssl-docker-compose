#!/bin/bash

DESTINATION=$1

# clone Flectra directory
git clone --depth=1 https://gitlab.com/6ministers/business-apps/onlyoffice-ssl-docker-compose $DESTINATION
rm -rf $DESTINATION/.git

